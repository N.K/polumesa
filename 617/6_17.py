import cv2
import glob
import sys
import os
import numpy as np
from PIL import Image


def quantization(array,q,width,height):
    for i in range(0,width):
                for j in range(0,height):   #quantization
                    array[i,j]=array[i,j]/q
    np.round(array)

    encode(array,width,height)


def encode(array,width,height):
    
    for i in range(0,width):
        for j in range(0,height):
            if array[i][j]!= array[i][j-1]  or i==0 and j==0:
                
                code=(i,j,array[i][j])
                en.write(str(code))
    en.write("\n")


vidcap = cv2.VideoCapture('a.webm')
success,image = vidcap.read()
count = 0
a= input('Give quantization:')
q=int(a)

while success:
    cv2.imwrite("frame%d.jpg" % count, image)     # save frame as JPEG file
    success,image = vidcap.read()
    print('Read a new frame: ', success)
    count += 1



i=0
with open('encode.txt', 'w') as en:
    for frames in glob.glob("*.jpg"):
        print(i,"frame")
        image=Image.open(frames)
        width, height = image.size

        pix_val = list(image.getdata())

        red = [item[0] for item in pix_val] #take the colour components that i want(rgb)
        green = [item[1] for item in pix_val]
        blue= [item[2] for item in pix_val]
        red = np.array(red).reshape(width,height) #transform the list to a 2d array as the width and the height of the image
        green = np.array(green).reshape(width,height)
        blue = np.array(blue).reshape(width,height)
        width, height = image.size
        if i==0:
            final_red=red
            final_green=green
            final_blue=blue
      
        else:
            final_red=final_red-red
            final_green=final_green-green
            final_blue=final_blue-blue
            quantization(final_red,q,width,height)
            quantization(final_green,q,width,height)
            quantization(final_blue,q,width,height)

        
        i=i+1

en.close
or_size=((width*height)*3)*i
en_size=os.path.getsize('encode.txt')
logos=or_size/en_size
print(logos)

dir = os.path.dirname(os.path.realpath(__file__))


for zippath in glob.iglob(os.path.join(dir, '*.jpg')):
    os.remove(zippath)
